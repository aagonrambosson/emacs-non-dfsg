#!/bin/sh

tmpfile=$(mktemp)

cat debian/non-dfsg-files debian/common-files > $tmpfile

find . -type f -not -path '\./.git/*' -not -path '\./debian*' | fgrep -v -w -f $tmpfile | xargs rm -f
find . -type d -empty -delete

rm $tmpfile

